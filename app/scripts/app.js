'use strict';

/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.
 */
angular
  .module('kolotapp', [
    'ui.router',
    'ngAnimate'
  ])
  .config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.when('/dashboard', '/dashboard/semester');
    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
      })
        .state('login', {
          url: '/login',
          parent: 'base',
          templateUrl: 'views/login.html',
          controller: 'LoginCtrl'
        })
        .state('dashboard', {
          url: '/dashboard',
          parent: 'base',
          templateUrl: 'views/dashboard.html',
          controller: 'DashboardCtrl'
        })
          .state('semester', {
            url: '/semester',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/myclasses.html'
          })
          .state('class', {
            url: '/class',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/classes_this_semester.html',
            controller: 'classesThisSemesterCtrl'
          })
          .state('engine', {
            url: '/engine',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/engine.html',
            controller: 'engineCtrl'
          })
          .state('today', {
            url: '/today',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/today.html'
          })
          .state('demo', {
            url: '/demo',
            parent: 'dashboard',
            templateUrl: 'views/dashboard/demo.html',
            controller : 'demoCtrl'
          });
  });
