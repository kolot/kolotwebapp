angular.module('kolotapp')
  .controller('demoCtrl', ['$scope', 'wolframalpha', function($scope, wolframalpha) {
  	$scope.popcorn = null;
  	$scope.init = function () {
  	  
      wolframalpha.query("periodic table", function(result){
        var imageObj = {
          start: 5,
          end: 14,
          href : encodeURIComponent("http://www.wolframalpha.com/input/?i=" + "periodic table"),
          src: result.src,
          target: "displaydiv"
        };

        popcorn.image(imageObj);
      });
      
      wolframalpha.query("periodic table", function(result){
        var imageObj = {
          start: 49,
          end: 64,
          href : encodeURIComponent("http://www.wolframalpha.com/input/?i=" + "periodic table"),
          src: result.src,
          target: "displaydiv"
        };

        popcorn.image(imageObj);
      });

      wolframalpha.query("periodic table", function(result){
        var imageObj = {
          start: 104,
          end: 149,
          href : encodeURIComponent("http://www.wolframalpha.com/input/?i=" + "periodic table"),
          src: result.src,
          target: "displaydiv"
        };

        popcorn.image(imageObj);
      });

      wolframalpha.query("gallium", function(result){
        var imageObj = {
          start: 65,
          end : 94,
          href : encodeURIComponent("http://www.wolframalpha.com/input/?i=" + "gallium"),
          src: result.src,
          target: "displaydiv"
        };

        popcorn.image(imageObj);
      });

      wolframalpha.query("alkali metals", function(result){
        var imageObj = {
          start: 149,
          end : 180,
          href : encodeURIComponent("http://www.wolframalpha.com/input/?i=" + "alkali metals"),
          src: result.src,
          target: "displaydiv"
        };

        popcorn.image(imageObj);
      });

      var popcorn = Popcorn("#player");

  		var wikipediaDataList = [];
      var wolframalphaDataList = [];

  		wikipediaDataList.push({
         start: 14,
         end: 38,
         title: "Wikipedia: The Disappearing Spoon",
         src : "https://en.wikipedia.org/wiki/The_Disappearing_Spoon",
         target: "displaydiv",
      });

      wikipediaDataList.push({
         start: 38,
         end: 49,
         title: "Wikipedia: Robert Bunsen",
         src : "https://en.wikipedia.org/wiki/Robert_Bunsen",
         target: "displaydiv",
      });

      wikipediaDataList.push({
         start: 94,
         end: 104,
         title: "Wikipedia: Radioactive decay",
         src : "https://en.wikipedia.org/wiki/Radioactive_decay",
         target: "displaydiv",
      });
      

  		Popcorn.forEach(wikipediaDataList, function(dataItem){
  			popcorn.wikipedia(dataItem);
  		});

  		popcorn.play();
  		$scope.popcorn = popcorn;
  	}
    	$scope.sourceUrl = "audio/TheFlippedClassroomModel.mp3";
}]);

function wolfram(term){

}