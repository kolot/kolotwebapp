angular.module('kolotapp')
  .controller('classesThisSemesterCtrl', function($scope, $state) {

    var twoDaysAgo = new Date();
    twoDaysAgo.setDate(twoDaysAgo.getDate() - 2);
    $scope.time = twoDaysAgo.toDateString();

  });
