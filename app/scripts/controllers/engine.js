angular.module('kolotapp')
  .controller('engineCtrl', function($scope) {
	$scope.popcorn = null;
	$scope.init = function () {
  		var popcorn = Popcorn("#player");

		var dataList = [];

		dataList.push({
           start: 8,
           end: 45,
           title: "This is an article",
           src : "https://en.wikipedia.org/classroom",
           target: "displaydiv",
     	});
     	dataList.push({
           start: 45,
           end: 74,
           title: "This is an article",
           src : "https://en.wikipedia.org/homework",
           target: "displaydiv"
     	});
		dataList.push({
           start: 74,
           end: 109,
           title: "This is an article",
           src : "https://en.wikipedia.org/flipped_classroom",
           target: "displaydiv"
     	});
		dataList.push({
           start: 109,
           end: 136,
           title: "This is an article",
           src : "https://en.wikipedia.org/moodle",
           target: "displaydiv"
     	});
		dataList.push({
           start: 136,
           end: 152,
           title: "This is an article",
           src : "https://en.wikipedia.org/teacher",
           target: "displaydiv"
     	});
     	dataList.push({
           start: 152,
           end: 170,
           title: "This is an article",
           src : "https://en.wikipedia.org/technology",
           target: "displaydiv"
     	});

		Popcorn.forEach(dataList, function(dataItem){
			popcorn.wikipedia(dataItem);
		});
		popcorn.play();
		$scope.popcorn = popcorn;
	}
  	$scope.sourceUrl = "audio/TheFlippedClassroomModel.mp3";
  });
