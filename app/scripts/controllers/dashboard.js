'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('kolotapp')
  .controller('DashboardCtrl', function($scope, $state) {

    $scope.$state = $state;

  });
