angular.module('kolotapp')
	.factory('wolframalpha', ['$http', function($http) {
   		var query = function(searchTerm, callback){
   			console.log("Inside query function--------------");
   			var url = "/api/wolframalpha/" + encodeURIComponent(searchTerm); 
			$http({
  				method: 'GET',
  				url: url
			}).then(function successCallback(response){
				callback(response.data);
			}, 
			function errorCallback(response) {
			  	console.log(response);
			  });
   		};

   		var queryResult = "";

   		var template = '<img src="{{src}}">';
   		return {
   			query : query,
   			result : queryResult
   		};
	}]);